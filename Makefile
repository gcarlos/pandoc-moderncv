PANDOC := pandoc
PANDOCFLAGS := -F ./moderncv.py --data-dir=. --latex-engine=xelatex
TARGETS := resume.pdf

all: $(TARGETS)

%.pdf %.tex: %.rst
	$(PANDOC) $(PANDOCFLAGS) -o $@ $<

clean:
	$(RM) $(TARGETS)
.PHONY: clean
