pandoc-moderncv (the real one)
==============================

Requirements
------------

- Haskell
- Python
- Pandoc
- pandocfilters (pipy)

Usage
-----

`make resume.pdf` or `make resume.tex`

Known Issues
------------

* Can't use blocks (lists, tables, etc) as arguments to moderncv commands
