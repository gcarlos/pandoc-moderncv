#!/usr/bin/env python

from collections import Counter
from itertools import chain, zip_longest
from pandocfilters import Plain, RawInline, toJSONFilter


CMDS = (None, None, 'cvline', 'cvlanguage', 'cvcomputer', None, 'cventry')
CMDS_ARGS = {'cventry': ('header',
                         'title', 'employer', 'city', 'extra', 'description'),
             'cvlanguage': ('header',
                            'level', 'extra')}
SYNONYMS = {'institution': 'employer',
            'degree': 'title',
            'grade': 'extra',
            'desc': 'description'}
AST_BLOCKS = ('Plain', 'Para', 'CodeBlock', 'RawBlock', 'BlockQuote',
              'OrderedList', 'BulletList', 'DefinitionList', 'Header',
              'HorizontalRule', 'Table', 'Div', 'Null')


def inline(e):
    """Transform pandoc AST block nodes into a list of inline nodes"""
    def _inline(x):
        if isinstance(x, (list, tuple)):
            return list(map(inline, x))
        if isinstance(x, str):
            return [{'c': x, 't': 'Str'}]
        if not e:
            return [{'c': '', 't': 'Str'}]
        if x['t'] in AST_BLOCKS:
            return inline(x['c'])
        return [x]
    if isinstance(e, (list, tuple)):
        return lchain(*_inline(e))
    return _inline(e)


latex = lambda s: RawInline('latex', s)
mk_arg = lambda a: [latex('{')] + inline(a) + [latex('}')]
lchain = lambda *a: list(chain(*a))


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def cv_from_args_num(*args):
    """Generate moderncv commands based on the number of arguments"""
    cmd = CMDS[len(args)]
    return Plain([latex('\\' + cmd)] + lchain(*map(mk_arg, args)))


def guess_cmd(keys):
    """Guess the moderncv command to use based on the arguments names"""
    cmds = {cmd: len(keys & set(args)) for cmd, args in CMDS_ARGS.items()}
    return Counter(cmds).most_common()[0][0]


def cv_from_args_name(*args):
    """Generate moderncv commands based on the arguments names"""
    largs = list(args)
    largs[0] = inline('header'), args[0]
    dargs = {}
    for k, v in largs:
        key = k[0]['c'].lower()
        canonical_key = key if key not in SYNONYMS else SYNONYMS[key]
        dargs[canonical_key] = v
    cmd = guess_cmd(dargs.keys())
    return Plain([latex('\\' + cmd)] +
                 lchain(*[mk_arg(dargs.get(n)) for n in CMDS_ARGS[cmd]]))


def deflists(key, value, format, meta):
    if format != 'latex':
        return
    ret = []
    if key == 'BlockQuote':
        if value[0]['t'] == 'DefinitionList':
            for args in grouper(chain(*value[0]['c']), 4):
                ret.append(cv_from_args_num(*args))
        return ret
    elif key == 'DefinitionList':
        for header, rest in value:
            args = rest[0][0]
            if args['t'] == 'Para':
                ret.append(cv_from_args_num(header, rest))
            elif args['t'] == 'BulletList':
                ret.append(cv_from_args_num(header, *args['c']))
            elif args['t'] == 'DefinitionList':
                ret.append(cv_from_args_name(header, *args['c']))
        return ret


if __name__ == "__main__":
    toJSONFilter(deflists)

