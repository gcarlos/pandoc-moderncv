:FirstName: John
:LastName: Smith
:Address: Paris, France
:Mobile: `(000) 000-0000`
:Email: `john@smith.net`
:Quote: Bla bla bla

Experience
==========

Professional
------------

Year -- Year
  :title: Job Title
  :employer: Employer
  :city: City
  :desc: Description

Year -- Year
  :title: Job Title
  :employer: Employer
  :city: City
  :extra: Extra
  :desc: Description

Education
=========

Year -- Year
  :degree: Degree
  :institution: Institution
  :grade: Grade
  :desc: Description

Year -- Year
  :degree: Degree
  :institution: Institution

Skills
======

  :Category 1: **Skills** **Skills** Skills
  :Category 2: Skills Skills Skills

  :Category 3: Skills Skills Skills
  :Category 4: Skills Skills Skills

:Category 5: Skills
:Category 6: Skills

Languages
=========

English
  :level: Level
  :extra: Extra

Spanish
  :level: Level

